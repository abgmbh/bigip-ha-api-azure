# Setup Onboarding scripts
data "template_file" "vm_onboard" {
  template = file("${path.module}/onboard.tpl")

  vars = {
    admin_user     = var.username
    admin_password = var.password
    DO_URL         = var.DO_URL
    AS3_URL        = var.AS3_URL
    TS_URL         = var.TS_URL
    CF_URL         = var.CF_URL
    libs_dir       = var.libs_dir
    onboard_log    = var.onboard_log
    mgmt_gw        = var.mgmt_gw
    f5vm01mgmt     = var.f5vm01mgmt
    f5vm02mgmt     = var.f5vm02mgmt
  }
}

data "template_file" "vm01_do_json" {
  template = file("${path.module}/do.json")

  vars = {
    host1           = var.host1_name
    host2           = var.host2_name
    local_host      = var.host1_name
    external_selfip = var.f5vm01ext
    remote_mgmtip   = var.f5vm02mgmt
    gateway         = var.ext_gw
    dns_server      = var.dns_server
    ntp_server      = var.ntp_server
    timezone        = var.timezone
    admin_user      = var.username
    admin_password  = var.password
    mgmt_gw         = var.mgmt_gw
  }
}

data "template_file" "vm02_do_json" {
  template = file("${path.module}/do.json")

  vars = {
    host1           = var.host1_name
    host2           = var.host2_name
    local_host      = var.host2_name
    external_selfip = var.f5vm02ext
    remote_mgmtip   = var.f5vm01mgmt
    gateway         = var.ext_gw
    dns_server      = var.dns_server
    ntp_server      = var.ntp_server
    timezone        = var.timezone
    admin_user      = var.username
    admin_password  = var.password
    mgmt_gw         = var.mgmt_gw
  }
}

data "template_file" "failover_json" {
  template = file("${path.module}/failover.json")

  vars = {
    f5_cloud_failover_label = var.f5_cloud_failover_label
    managed_route1          = var.managed_route1
    local_selfip            = var.f5vm02ext
    remote_selfip           = var.f5vm01ext
  }
}

resource "local_file" "vm_onboard_file" {
  content  = data.template_file.vm_onboard.rendered
  filename = "${path.module}/${var.vm_onboard_file}"
}

resource "local_file" "vm01_do_file" {
  content  = data.template_file.vm01_do_json.rendered
  filename = "${path.module}/${var.rest_vm01_do_file}"
}

resource "local_file" "vm02_do_file" {
  content  = data.template_file.vm02_do_json.rendered
  filename = "${path.module}/${var.rest_vm02_do_file}"
}

resource "local_file" "vm_failover_file" {
  content  = data.template_file.failover_json.rendered
  filename = "${path.module}/${var.rest_vm_failover_file}"
}



