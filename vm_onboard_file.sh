#!/bin/bash

# BIG-IPS ONBOARD SCRIPT

LOG_FILE=/var/log/startup-script.log

if [ ! -e $LOG_FILE ]
then
     touch $LOG_FILE
     exec &>>$LOG_FILE
else
    #if file exists, exit as only want to run once
    exit
fi

exec 1>$LOG_FILE 2>&1

# CHECK TO SEE NETWORK IS READY
CNT=0
while true
do
  STATUS=$(curl -s -k -I example.com | grep HTTP)
  if [[ $STATUS == *"200"* ]]; then
    echo "Got 200! VE is Ready!"
    break
  elif [ $CNT -le 6 ]; then
    echo "Status code: $STATUS  Not done yet..."
    CNT=$[$CNT+1]
  else
    echo "GIVE UP..."
    break
  fi
  sleep 10
done

sleep 60

tmsh modify /auth password-policy policy-enforcement disabled
tmsh modify /auth user admin password 'password'

tmsh modify sys db provision.extramb value 1000
tmsh modify sys db restjavad.useextramb value true

tmsh save sys config

bigstart restart restjavad restnoded

###############################################
#### Download F5 Automation Toolchain RPMs ####
###############################################

# Variables
admin_username='admin'
admin_password='password'
CREDS="admin:"$admin_password
DO_URL='https://github.com/F5Networks/f5-declarative-onboarding/releases/download/v1.29.0/f5-declarative-onboarding-1.29.0-8.noarch.rpm'
DO_FN=$(basename "$DO_URL")
AS3_URL='https://github.com/F5Networks/f5-appsvcs-extension/releases/download/v3.21.0/f5-appsvcs-3.21.0-4.noarch.rpm'
AS3_FN=$(basename "$AS3_URL")
TS_URL='https://github.com/F5Networks/f5-telemetry-streaming/releases/download/v1.13.0/f5-telemetry-1.13.0-2.noarch.rpm'
TS_FN=$(basename "$TS_URL")
CF_URL='https://github.com/F5Networks/f5-cloud-failover-extension/releases/download/v1.4.0/f5-cloud-failover-1.4.0-0.noarch.rpm'
CF_FN=$(basename "$CF_URL")


mkdir -p /config/cloud/azure/node_modules

echo -e "\n"$(date) "Download Telemetry (TS) Pkg"
curl -L -k -o /config/cloud/azure/node_modules/$TS_FN $TS_URL

echo -e "\n"$(date) "Download Declarative Onboarding (DO) Pkg"
curl -L -k -o /config/cloud/azure/node_modules/$DO_FN $DO_URL

echo -e "\n"$(date) "Download Application Services 3 (AS3) Pkg"
curl -L -k -o /config/cloud/azure/node_modules/$AS3_FN $AS3_URL

echo -e "\n"$(date) "Download Cloud Failover Extension (CFE) Pkg"
curl -L -k -o /config/cloud/azure/node_modules/$CF_FN $CF_URL

sleep 10

# Copy the RPM Pkg to the file location
cp /config/cloud/azure/node_modules/*.rpm /var/config/rest/downloads/

# Install Telemetry Streaming Pkg
DATA="{\"operation\":\"INSTALL\",\"packageFilePath\":\"/var/config/rest/downloads/$TS_FN\"}"
echo -e "\n"$(date) "Install TS Pkg"
curl -u $CREDS -X POST http://localhost:8100/mgmt/shared/iapp/package-management-tasks -d $DATA

sleep 10

# Install Declarative Onboarding Pkg
DATA="{\"operation\":\"INSTALL\",\"packageFilePath\":\"/var/config/rest/downloads/$DO_FN\"}"
echo -e "\n"$(date) "Install DO Pkg"
curl -u $CREDS -X POST http://localhost:8100/mgmt/shared/iapp/package-management-tasks -d $DATA

sleep 10

# Install AS3 Pkg
DATA="{\"operation\":\"INSTALL\",\"packageFilePath\":\"/var/config/rest/downloads/$AS3_FN\"}"
echo -e "\n"$(date) "Install AS3 Pkg"
curl -u $CREDS -X POST http://localhost:8100/mgmt/shared/iapp/package-management-tasks -d $DATA

sleep 10

# Install CF Pkg
DATA="{\"operation\":\"INSTALL\",\"packageFilePath\":\"/var/config/rest/downloads/$CF_FN\"}"
echo -e "\n"$(date) "Install CF Pkg"
curl -u $CREDS -X POST http://localhost:8100/mgmt/shared/iapp/package-management-tasks -d $DATA

sleep 10

# Check DO Ready
CNT=0
echo -e "\n"$(date) "Check DO Ready"
while true
do
  STATUS=$(curl -u $CREDS -X GET -s -k -I https://localhost/mgmt/shared/declarative-onboarding/info | grep HTTP)
  if [[ $STATUS == *"200"* ]]; then
    echo -e "\n"$(date) "Got 200! DO is Ready!"
    break
  elif [ $CNT -le 6 ]; then
    echo -e "\n"$(date) "Status code: $STATUS  DO Not done yet..."
    CNT=$[$CNT+1]
  else
    echo -e "\n"$(date) "(DO) GIVE UP..."
    break
  fi
  sleep 10
done

# Check AS3 Ready
CNT=0
echo -e "\n"$(date) "Check AS3 Ready"
while true
do
  STATUS=$(curl -u $CREDS -X GET -s -k -I https://localhost/mgmt/shared/appsvcs/info | grep HTTP)
  if [[ $STATUS == *"200"* ]]; then
    echo -e "\n"$(date) "Got 200! AS3 is Ready!"
    break
  elif [ $CNT -le 6 ]; then
    echo -e "\n"$(date) "Status code: $STATUS  AS3 Not done yet..."
    CNT=$[$CNT+1]
  else
    echo -e "\n"$(date) "(AS3) GIVE UP..."
    break
  fi
  sleep 10
done

# Check TS Ready
CNT=0
echo -e "\n"$(date) "Check TS Ready"
while true
do
  STATUS=$(curl -u $CREDS -X GET -s -k -I https://localhost/mgmt/shared/telemetry/info | grep HTTP)
  if [[ $STATUS == *"200"* ]]; then
    echo -e "\n"$(date) "Got 200! TS is Ready!"
    break
  elif [ $CNT -le 6 ]; then
    echo -e "\n"$(date) "Status code: $STATUS  TS Not done yet..."
    CNT=$[$CNT+1]
  else
    echo -e "\n"$(date) "(TS) GIVE UP..."
    break
  fi
  sleep 10
done

# Check CF Ready
CNT=0
echo -e "\n"$(date) "Check CF Ready"
while true
do
  STATUS=$(curl -u $CREDS -X GET -s -k -I https://localhost/mgmt/shared/cloud-failover/info | grep HTTP)
  if [[ $STATUS == *"200"* ]]; then
    echo -e "\n"$(date) "Got 200! CF is Ready!"
    break
  elif [ $CNT -le 6 ]; then
    echo -e "\n"$(date) "Status code: $STATUS  CF Not done yet..."
    CNT=$[$CNT+1]
  else
    echo -e "\n"$(date) "(CF) GIVE UP..."
    break
  fi
  sleep 10
done

# Delete RPM packages
echo -e "\n"$(date) "Removing temporary RPM install packages"
rm -rf /var/config/rest/downloads/*.rpm

sleep 5
