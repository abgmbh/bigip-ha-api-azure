provider "azurerm" {
  features {}
  client_id                   = var.azure_client_id
  client_certificate_path     = var.client_certificate_path
  client_certificate_password = var.client_certificate_password
  subscription_id             = var.azure_subscription_id
  tenant_id                   = var.azure_tenant_id
}

resource "random_id" "id" {
  byte_length = 2
}

# Create a Resource Group
resource "azurerm_resource_group" "main" {
  name     = format("%s-rg-%s", var.prefix, random_id.id.hex)
  location = var.location
}

# Create the Storage Account
resource "azurerm_storage_account" "mystorage" {
  name                     = "czstorage101"
  resource_group_name      = azurerm_resource_group.main.name
  location                 = azurerm_resource_group.main.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = {
    f5_cloud_failover_label = var.f5_cloud_failover_label
  }
}
